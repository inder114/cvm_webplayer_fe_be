"use strict";
exports.__esModule = true;
var mse_1 = require("./mse");
window.addEventListener('load', function (w) {
    var videoTag = document.querySelectorAll('video');
    videoTag.forEach(function (vidElement) {
        vidElement.onerror = function () {
            if (vidElement.error != null) {
                console.log("Error " + vidElement.error.code + "; details: " + vidElement.error.message);
            }
        };
        var socketUrl = 'ws://localhost:8187';
        var src = vidElement.currentSrc;
        var mimeCodec = vidElement.getAttribute("codec");
        var encodeSrc = src;
        if (src.charAt(0) != 'r')
            encodeSrc = src.substring(src.indexOf('/', 8) + 1);
        var myMse = new mse_1.mse(vidElement, mimeCodec, socketUrl + "/" + encodeSrc);
        // if(mimeCodec.split("h").length >= 2)
        // {
        //   myMse.TranscodeInit(encodeSrc,"rtsp://localhost:8182/transcoded");
        //   myMse.socketUrl = socketUrl + "/" +"rtsp://localhost:8182/transcoded";
        //   myMse.mimeCodec = 'video/mp4; codecs="avc1.4d001f"';
        // }
        if (window.MediaSource && MediaSource.isTypeSupported(myMse.mimeCodec)) {
            myMse.videoElement.src = URL.createObjectURL(myMse.mediaSource);
            myMse.mediaSource.addEventListener('sourceopen', sourceOpen);
        }
        else {
            console.log("The Media Source Extensions API is not supported.");
        }
        // function sourceOpen(e:any) {
        //   var mediaSource = e.currentTarget;
        //   let sourceBuffer = mediaSource.addSourceBuffer(myMse.mimeCodec);  // add mime and codec value in source buffer.
        //   sourceBuffer.mode = 'sequence';
        //   myMse.fetchAB(myMse.socketUrl);
        //   myMse.appendBufferToSource(sourceBuffer);
        // }
        function sourceOpen(e) {
            var mediaSource = e.currentTarget;
            // if (mediaSource.sourceBuffers.length == 0) {
            var sourceBuffer = mediaSource.addSourceBuffer(myMse.mimeCodec); // add mime and codec value in source buffer.
            sourceBuffer.mode = 'sequence';
            // sourceBuffer.mode = 'segments';
            // sourceBuffer.onupdateend = function () {
            //   mediaSource.endOfStream();
            //   vidElement.play();
            // };
            myMse.fetchAB(myMse.socketUrl);
            myMse.appendBufferToSource(sourceBuffer);
            sleep(1);
            // }
            // else
            // {
            //   console.warn("Source buffer already present");
            // }
        }
        function sleep(ms) {
            return new Promise(function (resolve) { return setTimeout(resolve, ms); });
        }
        console.log({
            SourceBuffer: SourceBuffer,
            MediaSource: MediaSource,
            vidElement: vidElement
        });
    });
});
