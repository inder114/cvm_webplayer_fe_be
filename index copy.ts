
const fragmentQueue : any[] = [];
let streamStart: boolean = false;
let bufferCount:number = 0;

function fetchAB (url: string) {
    const socket = new WebSocket(url);
    socket.binaryType = 'arraybuffer';
    socket.onopen = () => {
        console.log("connected");
    };
    socket.onmessage = (e) => {
        fragmentQueue.push(e.data);
        // appendBufferToSource(sourceBuffer);
        // console.log(e.data);
    };
    socket.onclose = () => {
        console.log("closed");
    };
    socket.onerror = (e) => {
        console.log(e.type);
    };
};

const videoTag = document.querySelectorAll('video');
videoTag.forEach((vidElement)=>
{
  let socketUrl = 'ws://localhost:8182';
  
  let src: string = (vidElement as HTMLMediaElement).currentSrc;
  // let encodeSrc = src;
  // if(src.charAt(0)!= 'r')
  //    encodeSrc = src.substring(src.indexOf('/',8)+1);
  // socketUrl = socketUrl + "/" + encodeSrc;
  // console.log(encodeSrc);
  
  var mimeCodec = 'video/mp4; codecs="avc1.4D4028, mp4a.40.2"';
  // var mimeCodec = 'video/mp4; codecs="hev1.1.6.L123.b0, mp4a.40.2"';


  if (window.MediaSource && MediaSource.isTypeSupported(mimeCodec)) {
    var mediaSource = new MediaSource();

    // myMse.videoElement.src = URL.createObjectURL(myMse.mediaSource);
    //         myMse.mediaSource.addEventListener('sourceopen', sourceOpen);
    vidElement.src = URL.createObjectURL(mediaSource);  // Set mediaSource as the source of this video tag.
    mediaSource.addEventListener('sourceopen', sourceOpen);
  } else {
    console.log("The Media Source Extensions API is not supported.")
  }

  function sourceOpen(e:any) {
    var mime = 'video/mp4; codecs="avc1.4D401F"';
    // var mime = 'video/mp4; codecs="hev1.1.6.L123.b0, mp4a.40.2"';
    var mediaSource = e.target;
    // var mediaSource = e;
    let sourceBuffer = mediaSource.addSourceBuffer(mime);  // add mime and codec value in source buffer.
    
    // myMse.fetchAB(myMse.socketUrl);
    // myMse.appendBufferToSource(sourceBuffer);

    fetchAB(socketUrl);
    appendBufferToSource(sourceBuffer);
  }

  async function appendBufferToSource(sourceBuffer: any){
    while (true) {
        await sleep(2);
        // await removeBuffer(sourceBuffer, bufferCount).then((value)=>{
        //   bufferCount=value as number;
        //   });
        if (fragmentQueue.length>0 && !sourceBuffer.updating) {
          
            const data = fragmentQueue.shift() as any;

            const videoData = await data;  
            // console.log(videoData);  

            sourceBuffer.appendBuffer(videoData);
            bufferCount++;

            if (vidElement.buffered.length>0 && !streamStart){
                console.log("Video Buffered End Time: " + vidElement.buffered.end(0));
                vidElement.currentTime = vidElement.buffered.end(0);
                console.log("Current Time: " + vidElement.currentTime);
                streamStart=true;
            }
            else if (streamStart && vidElement.buffered.end(0)-vidElement.currentTime>0.6) {
                console.log(vidElement.buffered.end(0));
                console.log(vidElement.currentTime);
                vidElement.currentTime = vidElement.buffered.end(0);
            }         
          // vidElement?.play();
        }
    //}
    }
  }

function sleep(interval: number) {
  return new Promise((resolve) => {
    setTimeout(() => {
        resolve(null);
    }, interval)
})
}
 });
// function removeBuffer(sourceBuffer: any, bufferCount: any) {
//   return new Promise((resolve) =>{
//     if (bufferCount== maxBufferCount && !sourceBuffer.updating){
//         sourceBuffer.remove(0,vidElement.buffered.end(0)-loadedDuration);
//         bufferCount=0;
//     }
//     resolve(bufferCount);
// })
//}

