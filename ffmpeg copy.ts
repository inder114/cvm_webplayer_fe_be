import { spawn } from "child_process";
import { promisify } from "util";
import { WebSocket, WebSocketServer } from "ws";
const MP4Box = require('./mp4box.all.js');


export class ffmpeg{
fragmentQueue: any[];
rtspLink: string;
sleep = promisify(setTimeout);
frag_duration: string = "100000";
wsConnList: Map<number, WebSocket>;
pid: number;
isProcess:boolean;
ftyp: any = null;
moov: any = null;
moof: any = null;
wsConnDataRecord: Map<number, number>;
headersNotPresent:boolean;

constructor(rtspLink: string)
{
    this.rtspLink = rtspLink;
    this.fragmentQueue = Array();
    this.wsConnList = new Map<number, WebSocket>();
    this.pid = 0;
    this.isProcess = true;
    this.wsConnDataRecord = new Map<number, number>();
    this.headersNotPresent = true;
}

start()
{
    console.log("Running ffmpeg ");
    console.log("spawning - ", this.rtspLink);
    const ffmpegProc = spawn("ffmpeg", [
        "-stream_loop", "-1",
        "-i", this.rtspLink,
        "-c:v", "copy",                                              //Tell FFmpeg to copy the video stream as is (without decoding and encoding)                                                               
        "-an",                                                       //No audio
        "-movflags", "frag_keyframe+empty_moov+default_base_moof",
        "-frag_duration", "50000",                        //Create fragments that are duration microseconds long
        "-fflags", "nobuffer",
        "-tune", "zerolatency",
        "-f", "mp4",                                                 //Define pipe format to be mp4
        "-"
    ]);

    this.pid = ffmpegProc.pid!;
    console.log(this.pid);
       
    ffmpegProc.stdout.on("data", (data) => {
        // ws.send(data);
        this.fragmentQueue.push(data);
        this.saveInitialFragment(data.buffer);
    });
}    

exit()
{
    if(this.wsConnList.size == 0)
    {
        console.log("Inside exit");
        process.kill(this.pid!);
        this.isProcess = false;
    }
}
    
   async sendBufferData(){
       await this.sleep(100);
        while(this.isProcess) {
            this.exit();
            if (this.fragmentQueue.length == 0) {
                await this.sleep(100);
            }
            const data = this.fragmentQueue.shift();
            for (let [key,wsConn] of this.wsConnList)
            {   if(this.headersNotPresent)
                {
                    if (this.wsConnDataRecord.get(key)==0){
                        if (this.ftyp!=null){
                            wsConn.send(this.ftyp);
                            this.wsConnDataRecord.set(key, 1);
                        }                
                    }
                    else if (this.wsConnDataRecord.get(key)==1){
                        if (this.moov!=null){
                            wsConn.send(this.moov);
                        }
                        this.wsConnDataRecord.set(key, 2);
                    }
                    else if (this.wsConnDataRecord.get(key)==2){
                        if (this.getNameOfBox(data)=="moof" && this.hasFirstSampleFlag(new Uint8Array(data))){ 
                            console.log("got special moof");
                            wsConn.send(data);
                            this.wsConnDataRecord.set(key, 3);
                            this.headersNotPresent = false;
                        }
                    }
                }
            // console.log(data);
            // this.socket.send(data);
                wsConn.send(data);
            }
            await this.sleep(1);
        }
    };

    saveInitialFragment(data: any){
        if (this.moof==null){
            if (this.getNameOfBox(data)=="ftyp") {
                console.log("got ftyp");
                this.ftyp=data;
            }
            else if (this.getNameOfBox(data)=="moov") {
                console.log("got moov");
                this.moov=data;
            }
            else if (this.getNameOfBox(data)=="moof") {
                console.log("got moof");
                this.moof=data;
            }
        }
    }
        /*
    * Get ftyp, moov, moof box
    */

        getNameOfBox(data:any){
            try {
                let packet = new Uint8Array(data);
                let res = this.getBox(packet, 0);
                return res[1];
            }
            catch (e){
                console.log(e);
                return null;
            }
        }
    
        toInt(arr:any, index:number) { // From bytes to big-endian 32-bit integer.  Input: Uint8Array, index
            let dv = new DataView(arr.buffer, 0);
            //dv.byteLength // debug and check.
            return dv.getInt32(index, false); // big endian
        }
    
        bytesToString(arr:any, fr:number, to:number) { // From bytes to string.  Input: Uint8Array, start index, stop index.
            return String.fromCharCode.apply(null, arr.slice(fr,to));
        }
    
        getBox(arr:any, i:number) { // input Uint8Array, start index
            return [this.toInt(arr, i), this.bytesToString(arr, i+4, i+8)]
        }
    
        getSubBox(arr:any, box_name:any) { // input Uint8Array, box name
            let i = 0;
            let res = this.getBox(arr, i);
            let main_length = res[0]; let name = res[1]; // this boxes length and name
            i = i + 8;
    
            let sub_box = null;
    
            while (i < main_length) {
                res = this.getBox(arr, i);
                let l = res[0]; name = res[1];
    
                if (box_name == name) {
                    sub_box = arr.slice(i, i+Number(l))
                }
                i = i + Number(l);
            }
            return sub_box;
        }
    
        hasFirstSampleFlag(arr: any) { // input Uint8Array
            // [moof [mfhd] [traf [tfhd] [tfdt] [trun]]]
    
            let traf = this.getSubBox(arr, "traf");
            if (traf==null) { return false; }
    
            let trun = this.getSubBox(traf, "trun");
            if (trun==null) { return false; }
    
            // ISO/IEC 14496-12:2012(E) .. pages 5 and 57
            // bytes: (size 4), (name 4), (version 1 + tr_flags 3)
            let flags = trun.slice(10,13); // console.log(flags);
            let f = flags[1] & 4; // console.log(f);
            return f == 4;
        }
    
}


export class codec {
       
    rtspLink: string;
    sleep = promisify(setTimeout);
    codecInfo: string;
    pid:number;
    fragmentQueue: any[];
    stopProcess: boolean;
    filePos:number;
    mp4boxfile:any;
    wsConnList!: WebSocket;

    constructor(rtspLink:string)
    {
        this.rtspLink = rtspLink;
        this.codecInfo = "a";
        this.pid = 0;
        this.fragmentQueue = Array();
        this.stopProcess = false;
        this.filePos = 0;
        this.mp4boxfile = MP4Box.createFile();
    }

    start()
    {
        // var mp4boxfile = MP4Box.createFile(false);
        console.log("running command");
        console.log(this.rtspLink);
        const codecProc = spawn("./ffmpeg/ffmpeg", [
            "-stream_loop", "-1",
            "-i", this.rtspLink,
            "-c:v", "copy",                                              //Tell FFmpeg to copy the video stream as is (without decoding and encoding)                                                               
            "-an",                                                       //No audio
            "-movflags", "frag_keyframe+empty_moov+default_base_moof",
            "-frag_duration", "50000",                        //Create fragments that are duration microseconds long
            "-fflags", "nobuffer",
            "-tune", "zerolatency",
            "-f", "mp4",                                                 //Define pipe format to be mp4
            "-"
        ]);

        this.pid = codecProc.pid!;
        console.log(this.pid);
        codecProc.stdout.on("data", (data) => {
            // ws.send(data);
            this.fragmentQueue.push(data);
        });
    }

    exit()
    {
        console.log("Inside exit");
        process.kill(this.pid!);
        this.stopProcess = true;    
    }

    async getCodec()
    {
        this.onready(this);
        while (!this.stopProcess) {
            // console.log("Readable event");
            if (this.fragmentQueue.length == 0) {
                // console.log("queue empty");
                await this.sleep(100);
            }
            // console.log("queue length= ", this.fragmentQueue.length);
            const data = this.fragmentQueue.shift();
            // console.log("data :", data);
            if(data)
            {
            var ab = data.buffer;
            ab.fileStart = this.filePos;
            this.filePos += ab.byteLength;
            // console.log("data :", ab.byteLength);
            this.mp4boxfile.appendBuffer(ab);
            }
            else{
                this.mp4boxfile.flush();
            }
            await this.sleep(2);
        };
        return;
    }

    onready(e:codec)
    {
        this.mp4boxfile.onMoovStart = function () {
            console.log("Starting to receive File Information");
        }

        this.mp4boxfile.onSegment = function () {
            console.log("Starting to receive File Information");
        }

        this.mp4boxfile.onError = function() {
            console.log("error");
        }

        this.mp4boxfile.onReady = function (info:any) {
            var mime = 'video/mp4; codecs=\"';
            console.log("info  :" ,info);
            for (var i = 0; i < info.tracks.length; i++) {
                if (i !== 0) mime += ',';
                mime+= info.tracks[i].codec;
            }
            mime += '\"';
            console.log(mime);
            e.codecInfo = mime;
            e.stopProcess = true;
            e.wsConnList.send(e.codecInfo);
            e.wsConnList.close();
            e.exit();
        };
    }

    toArrayBuffer(buffer:any) {
        console.log(buffer ? buffer.length : 'json_data is null or undefined');
        var ab = new ArrayBuffer(buffer.length);
        var view = new Uint8Array(ab);
        for (var i = 0; i < buffer.length; ++i) {
            view[i] = buffer[i];
        }
        return ab;
    }

    typedArrayToBuffer(array: Uint8Array): ArrayBuffer {
        return array.buffer.slice(array.byteOffset, array.byteLength + array.byteOffset)
    }
}

var trueTypeOf = (obj:any) => Object.prototype.toString.call(obj).slice(8, -1).toLowerCase()


