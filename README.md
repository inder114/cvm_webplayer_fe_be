# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

Do npm i to install the dependency modules.

To run the backend -> run " tsc Backend-main.ts --downlevelIteration " .
This will compile the backend TS files into JS (needed to be done for mp4box.all.js)

Next, you can run the backend by running the following command - " node Backend-main.js"

To run the frontend -> we need to bundle the TS files into 1 JS file.
For doing this, run " npm run dev"

after that , just start the index.html by right click the file and selecting live server(install the plugin if you dont have it)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact