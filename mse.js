"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.mse = void 0;
var mse = /** @class */ (function () {
    function mse(videoElement, mimeCodec, socketUrl) {
        this.streamStart = false;
        this.bufferCount = 0;
        this.loadedDuration = 20;
        this.maxBufferCount = 600;
        this.videoElement = videoElement;
        this.mimeCodec = mimeCodec;
        this.mediaSource = new MediaSource();
        this.fragmentQueue = Array();
        this.socketUrl = socketUrl;
    }
    // TranscodeInit(url: string, newUrl: string) 
    // {
    //     const socket = new WebSocket("ws://localhost:8190/"+url);
    //     socket.binaryType = 'arraybuffer';
    //     socket.onopen = () => {
    //         console.log("connected");
    //         socket.send(JSON.stringify(["oldUrl", url])); //array 1
    //         socket.send(JSON.stringify(["Url", newUrl])); //array 2
    //     };
    //     socket.onmessage = (e) => {
    //         console.log("message recieved");
    //     };
    //     socket.onclose = () => {
    //         console.log("closed");
    //     };
    //     socket.onerror = (e) => {
    //         console.log(e.type);
    //     };  
    // }
    mse.prototype.fetchAB = function (url) {
        var _this = this;
        var socket = new WebSocket(url);
        socket.binaryType = 'arraybuffer';
        socket.onopen = function () {
            console.log("connected");
        };
        socket.onmessage = function (e) {
            _this.sleep(1);
            _this.fragmentQueue.push(e.data);
        };
        socket.onclose = function () {
            console.log("closed");
        };
        socket.onerror = function (e) {
            console.log(e.type);
        };
    };
    ;
    mse.prototype.appendBufferToSource = function (sourceBuffer) {
        return __awaiter(this, void 0, void 0, function () {
            var data, videoData;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!true) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.sleep(2)];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.removeBuffer(sourceBuffer, this.bufferCount).then(function (value) {
                                _this.bufferCount = value;
                            })];
                    case 2:
                        _a.sent();
                        if (!(this.fragmentQueue.length > 0 && !sourceBuffer.updating)) return [3 /*break*/, 4];
                        data = this.fragmentQueue.shift();
                        return [4 /*yield*/, data];
                    case 3:
                        videoData = _a.sent();
                        sourceBuffer.appendBuffer(videoData);
                        this.bufferCount++;
                        if (this.videoElement.buffered.length > 0 && !this.streamStart) {
                            this.videoElement.currentTime = this.videoElement.buffered.end(0);
                            this.streamStart = true;
                        }
                        else if (this.streamStart && this.videoElement.buffered.end(0) - this.videoElement.currentTime > 0.6) {
                            this.videoElement.currentTime = this.videoElement.buffered.end(0);
                        }
                        _a.label = 4;
                    case 4: return [3 /*break*/, 0];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    mse.prototype.removeBuffer = function (sourceBuffer, bufferCount) {
        var _this = this;
        return new Promise(function (resolve) {
            if (bufferCount == _this.maxBufferCount && !sourceBuffer.updating) {
                sourceBuffer.remove(0, _this.videoElement.buffered.end(0) - _this.loadedDuration);
                bufferCount = 0;
            }
            resolve(bufferCount);
        });
    };
    mse.prototype.sleep = function (interval) {
        return new Promise(function (resolve) {
            setTimeout(function () {
                resolve(null);
            }, interval);
        });
    };
    return mse;
}());
exports.mse = mse;
