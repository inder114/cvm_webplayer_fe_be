import { transcode } from "buffer";
import { mse } from "./mse";

window.addEventListener('load', (w) => {
  const videoTag = document.querySelectorAll('video');
  
  videoTag.forEach((vidElement?)=>
  {
    vidElement.onerror = function(){
      if (vidElement.error!=null){
          console.log("Error " + vidElement.error.code + "; details: " + vidElement.error.message);
      }
    };

    let socketUrl = 'ws://localhost:8187';
    let src: string = (vidElement as HTMLMediaElement).currentSrc;
    let mimeCodec: string = vidElement.getAttribute("codec")!;
    let encodeSrc = src
    if(src.charAt(0)!= 'r')
      encodeSrc = src.substring(src.indexOf('/',8)+1);

    let myMse = new mse(vidElement as HTMLMediaElement, mimeCodec, socketUrl + "/" + encodeSrc);

    // if(mimeCodec.split("h").length >= 2)
    // {
    //   myMse.TranscodeInit(encodeSrc,"rtsp://localhost:8182/transcoded");
    //   myMse.socketUrl = socketUrl + "/" +"rtsp://localhost:8182/transcoded";
    //   myMse.mimeCodec = 'video/mp4; codecs="avc1.4d001f"';
    // }
    
    if (window.MediaSource && MediaSource.isTypeSupported(myMse.mimeCodec)) {

      myMse.videoElement.src = URL.createObjectURL(myMse.mediaSource);
      myMse.mediaSource.addEventListener('sourceopen', sourceOpen);
    } else {
      console.log("The Media Source Extensions API is not supported.")
    }

    // function sourceOpen(e:any) {
    //   var mediaSource = e.currentTarget;
    //   let sourceBuffer = mediaSource.addSourceBuffer(myMse.mimeCodec);  // add mime and codec value in source buffer.
    //   sourceBuffer.mode = 'sequence';
    //   myMse.fetchAB(myMse.socketUrl);
    //   myMse.appendBufferToSource(sourceBuffer);
    // }
    function sourceOpen(e:any) {
      var mediaSource = e.currentTarget;

      let sourceBuffer = mediaSource.addSourceBuffer(myMse.mimeCodec);  // add mime and codec value in source buffer.
      sourceBuffer.mode = 'sequence';
      // sourceBuffer.addEventListener('updatestart', function(e1:any) { console.log('updatestart: ' + mediaSource.readyState +myMse.socketUrl); });
      // sourceBuffer.addEventListener('update', function(e1:any) { console.log('update: ' + mediaSource.readyState +myMse.socketUrl); });
      // sourceBuffer.addEventListener('updateend', function(e1:any) { console.log('updateend: ' + mediaSource.readyState +myMse.socketUrl); });
      sourceBuffer.addEventListener('error', function(e1:any) { console.log('error: ' + mediaSource.readyState +myMse.socketUrl +" " +e1.toString()) ; });
      sourceBuffer.addEventListener('abort', function(e1:any) { console.log('abort: ' + mediaSource.readyState +myMse.socketUrl +" " +e1.toString()); });
      myMse.fetchAB(myMse.socketUrl);
      sleep(10);
      myMse.appendBufferToSource(sourceBuffer);
    }
    function sleep(ms:any) {
      return new Promise(resolve => setTimeout(resolve, ms));
      }
    console.log({
      SourceBuffer,
      MediaSource,
      vidElement
    });
  });
});
