const path = require('path');

module.exports = {
  devtool: 'eval-source-map',
  entry: './index.ts',
  module: {
    rules: [
      {
        test: /\.ts$/,
        // test: /\.tsx?$/,
        use: 'ts-loader',
        include: [ path.resolve(__dirname)],
        exclude: /node_modules/
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  output: {
    // publicPath: 'public',
    filename: 'bundle.js',
    path: path.resolve(__dirname),
  },
};